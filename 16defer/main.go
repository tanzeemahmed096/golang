package main

import "fmt"

func main() {
	fmt.Println("Defer in golang")

	defer fmt.Println("one")
	defer counter()
	defer fmt.Println("two")
	fmt.Println("Hello")

}

func counter() {
	for i := 0; i < 4; i++ {
		defer fmt.Println(i)
	}
}
