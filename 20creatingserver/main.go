package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	fmt.Println("Creating server in golang")
	createUser()
}

func createUser() {
	const url = "https://movies-api-tanzeem.onrender.com/api/users/signup"

	requestBody := strings.NewReader(`
		{
			"email": "xyz@gmail.com",
			"password": "1234567"
		}
	`)

	res, err := http.Post(url, "application/json", requestBody)

	if err != nil {
		panic(err)
	}

	defer res.Body.Close()

	var resBodyReader strings.Builder

	byteCount, err := ioutil.ReadAll(res.Body)

	byteLen, err := resBodyReader.Write(byteCount)

	if err != nil {
		panic(err)
	}

	fmt.Println(string(byteCount))

	fmt.Println(byteLen)

	fmt.Println(resBodyReader.String())
}
