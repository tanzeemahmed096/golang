package main

import "fmt"

func main() {
	myNum := 40
	fmt.Println(myNum)

	var ptr *int
	fmt.Println(ptr)

	var ptr2 = &myNum

	fmt.Println(ptr2)
	fmt.Println(*ptr2)
	*ptr2 = *ptr2 * 2
	fmt.Println(myNum)
}
