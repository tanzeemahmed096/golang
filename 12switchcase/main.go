package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	fmt.Println("Switch case in golang")

	rand.Seed(time.Now().UnixNano())

	num := rand.Intn(6) + 1

	fmt.Println(num)

	switch num {
	case 1:
		fmt.Println("spot 1")
	case 2:
		fmt.Println("spot 2")
	case 3:
		fmt.Println("spot 3")
	case 4:
		fmt.Println("spot 4")
	case 5:
		fmt.Println("spot 5")
	case 6:
		fmt.Println("spot 6")
	}
}
