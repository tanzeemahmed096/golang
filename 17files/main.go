package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("Reading files in golang")
	content := "Hello World"

	file, err := os.Create("./demo.txt")

	// if err != nil {
	// 	panic(err)
	// }

	checkError(err)

	length, err := io.WriteString(file, content)

	checkError(err)

	fmt.Println(length)
	defer file.Close()

	readFile("./demo.txt")
}

func readFile(filepath string) {
	content, err := ioutil.ReadFile(filepath)
	checkError(err)
	fmt.Println(string(content))
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
