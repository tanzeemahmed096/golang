package main

import "fmt"

func main() {
	fmt.Println("Functions in golang")

	res := addVal(1, 2)

	fmt.Println(res)

	add, message := addVals(1, 2, 3, 4)

	fmt.Println(add, message)
}

func addVal(val1 int, val2 int) int {
	return val1 + val2
}

func addVals(values ...int) (int, string) {
	total := 0

	for _, val := range values {
		total += val
	}

	return total, "Hi there"
}
