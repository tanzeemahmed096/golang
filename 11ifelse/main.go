package main

import "fmt"

func main() {
	fmt.Println("if else in golang")

	myNum := 15

	if myNum%2 == 0 {
		fmt.Println("Num is even")
	} else {
		fmt.Println("Num is odd")
	}

	if num := 0; num == 0 {
		fmt.Println("Num is zero")
	} else {
		fmt.Println("Num is > zero")
	}
}
