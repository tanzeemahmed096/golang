package main

import (
	"fmt"
)

var Logged int = 1234

func main() {
	var username string = "Tanzeem"
	fmt.Println(username)
	fmt.Printf("Variable type: %T \n", username)

	var isSkip bool = false
	fmt.Println(isSkip)
	fmt.Printf("Variable type: %T \n", isSkip)

	var smallVal uint8 = 255
	fmt.Println(smallVal)
	fmt.Printf("Variable type: %T \n", smallVal)

	var floatVal float64 = 23.23431234421341234
	fmt.Println(floatVal)
	fmt.Printf("Variable type: %T \n", floatVal)

	var useremail = "tan@gmail.com"
	fmt.Println(useremail)

	userpass := 1234.023
	fmt.Println(userpass)

	var doing bool
	fmt.Println(doing)

	fmt.Println(Logged)
}
