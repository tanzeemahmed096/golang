package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Time now")

	dateTime := time.Now()
	fmt.Println(dateTime)
	fmt.Println(dateTime.Format("01-02-2006 15:04:05 Monday"))

	createdData := time.Date(2022, time.April, 23, 23, 0, 0, 0, time.UTC)
	fmt.Println(createdData)
}
