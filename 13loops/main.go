package main

import "fmt"

func main() {
	fmt.Println("Loops in go lang")

	// numInt := []int{1, 2, 3, 4, 5, 6}

	// for idx := 0; idx < len(numInt); idx++ {
	// 	fmt.Println(numInt[idx])
	// }

	// for _, value := range numInt {
	// 	fmt.Println(value)
	// }

	num := 2

	for num < 5 {

		if num == 3 {
			goto numSav
		}

		if num == 3 {
			num++
			continue
		}

		fmt.Println(num)
		num++
	}

numSav:
	fmt.Println("Hello World")
}
