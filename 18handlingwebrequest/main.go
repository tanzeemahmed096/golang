package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const url = "https://gitlab.com/tanzeemahmed096/trello-react-tanzeem"

func main() {
	fmt.Println("Handling web requests")

	res, err := http.Get(url)

	if err != nil {
		panic(err)
	}

	bytecontent, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err)
	}

	content := string(bytecontent)

	defer fmt.Println(res.StatusCode)

	defer fmt.Println(content)

	defer res.Body.Close()
}
