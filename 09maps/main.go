package main

import "fmt"

func main() {
	fmt.Println("Maps in golang")

	languages := make(map[string]string)

	languages["JS"] = "JavaScript"
	languages["RB"] = "Ruby"
	languages["PY"] = "Python"

	fmt.Println(languages)
	fmt.Println(languages["RB"])

	delete(languages, "RB")
	fmt.Println(languages)

	for key, value := range languages {
		fmt.Println(key, value)
	}
}
