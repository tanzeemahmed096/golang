package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("I am main")
	greeter()
	r := mux.NewRouter()
	r.HandleFunc("/", serve).Methods("GET")

	log.Fatal(http.ListenAndServe(":3000", r))
}

func greeter() {
	fmt.Println("Hello")
}

func serve(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("<h1>Welcome to http</h1>"))
}
