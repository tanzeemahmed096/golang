package main

import (
	"fmt"
	"net/url"
)

const myUrl string = "https://trello-clone-react-six.vercel.app/"

func main() {
	fmt.Println("Url in golang")

	res, _ := url.Parse(myUrl)

	// fmt.Println(res.Scheme)
	// fmt.Println(res.Host)
	// fmt.Println(res.Path)
	// fmt.Println(res.Port())
	// fmt.Println(res.RawQuery)

	qparams := res.Query()
	fmt.Println(qparams["Hello"])

	createUrl := &url.URL{
		Scheme:   "https",
		Host:     "lco.dev",
		Path:     "/tutcss",
		RawQuery: "user=hitesh",
	}

	partsOfUrl := createUrl.String()

	fmt.Println(partsOfUrl)

}
