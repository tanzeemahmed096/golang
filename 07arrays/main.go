package main

import (
	"fmt"
)

func main() {
	fmt.Println("Arrays practice")

	var list [4]int

	list[0] = 2
	list[1] = 4

	fmt.Println(list)
	fmt.Println(len(list))
}
