package main

import "fmt"

func main() {
	fmt.Println("methods in goland")

	userDetail := User{"XYZ", "abc@gmail.com", true, 20}
	fmt.Println(userDetail)

	userDetail.getName()

	fmt.Println(userDetail)
}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}

func (u User) getName() {
	u.Age = 22
	fmt.Println(u.Age)
}
