package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Slices practice")

	var fruits = []int{1, 2, 3, 5, 4}

	fmt.Println(len(fruits))
	fruits = append(fruits, 10, 40)
	fmt.Println(fruits)

	fruits = append(fruits[2:5])

	fmt.Println(fruits)

	sort.Ints(fruits)
	fmt.Println(sort.IntsAreSorted(fruits))

	highScores := make([]int, 4)

	highScores[0] = 1
	highScores[0] = 2
	highScores = append(highScores, 3, 4)

	var idx int = 1

	highScores = append(highScores[:idx], highScores[idx+3:]...)

	fmt.Println(highScores[2])
}
