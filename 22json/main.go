package main

import (
	"encoding/json"
	"fmt"
)

type data struct {
	Name      string   `json:"username"`
	Email     string   `json:"useremail"`
	Age       int      `json:"-"`
	Languages []string `json:"languages,omitempty"`
}

func main() {
	fmt.Println("Converting to json")
	// convertToJson()
	jsonTostruct()
}

func convertToJson() {
	user := []data{
		{"xyz", "xyz@gmail.com", 22, []string{"hindi", "english"}},
		{"abc", "abc@gmail.com", 23, []string{"english"}},
		{"xyz", "xyz@gmail.com", 22, nil},
	}

	jsonData, err := json.MarshalIndent(user, "", "\t")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", jsonData)
}

func jsonTostruct() {
	userdata := []byte(
		`
		[{
			"username": "xyz",
			"useremail": "xyz@gmail.com",
			"languages": [
					"hindi",
					"english"
			]
		},
		{
			"username": "abc",
			"useremail": "abc@gmail.com",
			"languages": [
					"english"
			]
		}]
		`,
	)

	var jsonWebData []data
	isValid := json.Valid(userdata)

	if isValid {
		fmt.Println("JSON is valid")
		json.Unmarshal(userdata, &jsonWebData)
		fmt.Printf("%#v\n", jsonWebData)
	} else {
		fmt.Println("JSON is not valid")
	}
}
