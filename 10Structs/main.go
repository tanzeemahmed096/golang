package main

import "fmt"

func main() {
	fmt.Println("Structs in golang")

	userDetail := User{"Tanzeem", "abc@gmail", true, 23}

	fmt.Printf("%+v", userDetail)
	fmt.Println(userDetail.Name)
	userDetail.Name = "Ahmed"
	fmt.Println(userDetail)
}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}
